node {
   def mvnHome = tool 'M3'
   
   def repoURL = 'https://FelixRivares@bitbucket.org/FelixRivares/testapplication.git'

   stage('Get from SCM') { // for display purposes
      echo "repository $repoURL"
      echo "pull-request branch - feature/sample"
      git "${repoURL}"
   }
   stage('Build artifact') {
      // Run the maven build
      //withEnv(["MVN_HOME=$mvnHome"]) {
      //  sh '"$MVN_HOME/bin/mvn" -Dmaven.test.failure.ignore clean package'
      //}
      echo "build with maven: -Dmaven.test.failure.ignore clean package"
      echo "build succeed"
   }
   stage('Unit testing') {
      echo "start unit testing"
      echo "testing finished, result:"
      echo "PASSED: 4"
      echo "FAILED: 1"
      echo "IGNORED: 1"
   }
   stage('Static code analysis') {
      echo "Code coverage: 83.6%"
      echo "Duplications: 0%"
      echo "Sequrity: 0 Vulnarabilities"
      echo "More detailed report available on: https://sonarcloud.io/dashboard?id=Chernov-Sergey_testapplication"

   }
   stage('Get author info') {
     echo "Load Pull-request author info..."
     echo "Founded employee with id #1212, reference on portal: http://localhost:8081/person_info"
   }
   stage('Get task info') {
     echo "Load task info..."
     echo "Send request to JIRA"
     echo "Task ID: JIRA-000"
   }
   stage('Generate report') {
     echo "Generating report..."
   }
   stage('Send report') {
      echo "Send report to reviewer report..."
   }
   stage('Results') {
      notifySuccessful()  
   }
}

def notifySuccessful() {
  emailext (
      subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: """
                SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\nFull report:"http://127.0.0.1:8081/"
            """,
        to: '$DEFAULT_RECIPIENTS'
    )
}